# Emprestimo
##### Author Leonardo Bugoni (leobugoni@gmail.com)

## Application MVC made with Spring Boot, Hsqldb, Liquibase, Hibernate, JPA, Junit and Vue.js


## Getting Started

#### Run backend server

```bash
mvn clean install -U -DskipTests
```

```bash
mvn spring-boot:run
```
##### Run frontend server

```bash
cd frontend/
```

```bash
npm install
```

```bash
npm run serve
```
  
Acessar: [localhost:4200](http://localhost:4200/)
