package io.github.leobugoni.emprestimo.repository;

import br.eti.clairton.repository.Repository;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;

@Component
public class RepositoryApp extends Repository {
	private static final long serialVersionUID = -4950370869128233506L;

	@Deprecated
	protected RepositoryApp() {
		this(null);
	}

	@Inject
	public RepositoryApp(@NotNull final EntityManager em) {
		super(em);
	}

	@Override
	public Repository readonly() {
		hint("org.hibernate.readOnly", "true");
		hint("org.hibernate.cacheable", "false");
		return this;
	}
}
