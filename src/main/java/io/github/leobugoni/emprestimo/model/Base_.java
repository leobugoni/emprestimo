package io.github.leobugoni.emprestimo.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Base.class)
public class Base_ {
    public static volatile SingularAttribute<Base, Long> id;
}
