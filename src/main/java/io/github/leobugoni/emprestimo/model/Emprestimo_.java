package io.github.leobugoni.emprestimo.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.math.BigDecimal;

@StaticMetamodel(Emprestimo.class)
public abstract class Emprestimo_ extends Base_{
    public static volatile SingularAttribute<Emprestimo, String> estado;
    public static volatile SingularAttribute<Emprestimo, BigDecimal> valor;
}