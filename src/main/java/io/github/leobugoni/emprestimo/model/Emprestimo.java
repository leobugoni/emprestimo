package io.github.leobugoni.emprestimo.model;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "emprestimos")
public class Emprestimo extends Base {

    @NotNull
    private String estado;

    @NotNull
    private BigDecimal valor;

    public Emprestimo() {
        super();
    }

    public Emprestimo(final String estado, final BigDecimal valor) {
        this();
        this.estado = estado;
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public BigDecimal getValor() {
        return valor;
    }
}
