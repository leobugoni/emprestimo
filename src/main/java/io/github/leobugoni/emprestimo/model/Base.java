package io.github.leobugoni.emprestimo.model;

import br.eti.clairton.identificator.Identificable;
import br.eti.clairton.identificator.Identificator;

import javax.persistence.Cacheable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

import static br.eti.clairton.identificator.Identificator.Type.TO_STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Cacheable
@MappedSuperclass
public class Base extends Identificable implements Serializable, Cloneable {

    @Id
    @Identificator(TO_STRING)
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        final Base entidade = (Base) super.clone();
        entidade.id = null;
        return entidade;
    }
}
