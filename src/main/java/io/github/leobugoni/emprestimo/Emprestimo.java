package io.github.leobugoni.emprestimo;

import javax.inject.Qualifier;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

@Inherited
@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, PARAMETER, METHOD, CONSTRUCTOR, TYPE })
public @interface Emprestimo {}
