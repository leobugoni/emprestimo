package io.github.leobugoni.emprestimo.controller;

import io.github.leobugoni.emprestimo.model.Emprestimo;
import io.github.leobugoni.emprestimo.repository.RepositoryApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@CrossOrigin
public class EmprestimoController {

    private final RepositoryApp repository;

    @Autowired
    public EmprestimoController(final RepositoryApp repository) {
        this.repository = repository;
    }


    @GetMapping(value = "/emprestimos")
    public List<Emprestimo> retriveAll() {
        final List<Emprestimo> emprestimos = repository.from(Emprestimo.class).list();
        emprestimos.forEach(this::mostraObj);
        return emprestimos;
    }

    @GetMapping(value = "/emprestimos/{id}")
    public Emprestimo retrive(@PathVariable final Long id) {
        final Emprestimo emprestimo = repository.byId(Emprestimo.class, id);
        mostraObj(emprestimo);
        return emprestimo;
    }

    @PostMapping(value = "/emprestimos")
    public void create(@RequestBody final Emprestimo emprestimo) {
        mostraObj(emprestimo);
        repository.save(emprestimo);
    }

    @DeleteMapping(value = "/emprestimos/{id}")
    public void delete(@PathVariable final Long id) {
        System.out.println("Deletando emprestimo " + id);
        repository.remove(Emprestimo.class, id);
    }

    private void mostraObj(final Emprestimo emprestimo){
        System.out.println("== Emprestimo ==");
        System.out.println("Id: " + emprestimo.getId());
        System.out.println("Estado: " + emprestimo.getEstado());
        System.out.println("Valor: " + emprestimo.getValor());
        System.out.println("================");
    }


}
