package io.github.leobugoni.emprestimo.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EmprestimoIntegrationTest extends Integration {

    @Test
    public void testRecuperarProduto() {
        final Emprestimo emprestimo = repository.from(Emprestimo.class).where("CONTRATATO", Emprestimo_.estado).first();
        assertEquals(2L, emprestimo.getId().longValue());
        assertEquals("CONTRATADO", emprestimo.getEstado());
        assertEquals(new BigDecimal("1100.00"), emprestimo.getValor());
    }
}