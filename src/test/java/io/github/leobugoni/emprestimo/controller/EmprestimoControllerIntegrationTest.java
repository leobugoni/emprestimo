package io.github.leobugoni.emprestimo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class EmprestimoControllerIntegrationTest extends ControllerIntegration {

    @Test
    public void testIndex() throws Exception {
        final MvcResult result = getMockMvc()
                .perform(get("/emprestimos"))
                .andReturn();
        System.out.println(result.getResponse().getContentAsString());

        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void testRetriveProdutoById() throws Exception {
        final MvcResult result = getMockMvc()
                .perform(get("/emprestimos/{id}", 1))
                .andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void testCriarProduto() throws Exception {
        json = "{estado:'EM_ANALISE',valor:0}";
        getMockMvc()
                .perform(post("/emprestimos")
                .contentType(APPLICATION_JSON)
                .content(json))
                .andExpect(status().is(200));
    }

    @Test
    public void testDeletearEmprestimo() throws Exception {
        final MvcResult result = getMockMvc()
                .perform(delete("/emprestimos/{id}", 1))
                .andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void testErrorWhenRetrive() throws Exception {
        final ResultActions result = getMockMvc()
                .perform(get("/wrongUrl/{id}", 1))
                .andExpect(status().is4xxClientError());
        assertNotNull(result);
    }
}