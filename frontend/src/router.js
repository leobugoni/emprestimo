import Vue from "vue";
import Router from "vue-router";
import CustomersList from "./components/CustomersList.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/emprestimos",
      name: "emprestimos",
      alias: "/emprestimo",
      component: CustomersList
    }
  ]
});